import firebase from 'firebase';
import  firestore  from 'firebase/firebase-firestore';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBCbSJp8ytYPWRRG7JWPXBEgqFh7tqFIYA",
    authDomain: "save-tools.firebaseapp.com",
    databaseURL: "https://save-tools.firebaseio.com",
    projectId: "save-tools",
    storageBucket: "save-tools.appspot.com",
    messagingSenderId: "13998567376",
    appId: "1:13998567376:web:7a2dcf177939732ad413d5",
    measurementId: "G-WBVTCT8E0R"
  };


    //   const firebaseApp = firebase.initializeApp(firebaseConfig);
    // Initialize Firebase
    const firebaseApp = firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    firebase.firestore()

 export default firebaseApp.firestore()