import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import es from 'vuetify/es5/locale/es';
import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#403E8C',
        secondary: '#082B59',
        accent: '#3C35F2',
        accent_2:'#F2894E',
        luz: 'rgb(230, 230, 255)',
        error: '#E63122',
        dark: '#0c0a1a',
        muted:'#C5CEE8'
      },
    //   light: {
    //     primary: '#082B59',
    //     sand: '#038C73',
    //     light: ' #E6E6FF',
    //     secondary: '#403E8C',
    //     accent: '#3C35F2',
    //     error: '#E63122',
    //     dark: '#0c0a1a'
    //   },
      // light: {
      //   primary: '#8BD2B2',
      //   secondary: '#CC7654',
      //   accent: '#FFB44F',
      //   error: '#b71c1c',
      //   dark: '#665E53'
      // },
    },
  },
  
  lang: {
      locales: { es },
      current: 'es',
    },
  icons: {
    iconfont: [
      'fa', 
      'mdi',
    //  ' mdiSvg',
    ]
    
  },
});
