import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';

import '@babel/polyfill'
import textArea from 'vue-textarea-autosize'
import firebase from 'firebase/app';
import 'firebase/auth'
Vue.config.productionTip = false

firebase.auth().onAuthStateChanged( user =>{
    new Vue({
      router,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
})


