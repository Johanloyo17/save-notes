import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Calendar from '../views/Calendary.vue'
import Sign_in from '../views/Sign_in.vue'
import Sign_up from '../views/Sign_up.vue'
import Panel from '../views/Panel.vue'

import firebase from 'firebase/app'
import 'firebase/auth'

Vue.use(VueRouter)

const router = new VueRouter({
  routes:[
  
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  

  {
    path: '/Sign_in',
    name: 'Sign_in',
    component: Sign_in
  },
  {
    path: '/Sign_up',
    name: 'Sign_up',
    component: Sign_up
  },
  {
    path: '/Calendar',
    name: 'Calendar',
    component: Calendar,
    meta:{
      isPrivate : true
    }
  },
  {
    path: '/panel',
    name: 'Panel',
    component: Panel,
    meta:{
      isPrivate : true
    }
  },
]
})

//Guards rutes
router.beforeEach((to, from, next) => {
  let isPrivate = to.matched.some((record) => record.meta.isPrivate)
  let isUser = firebase.auth().currentUser;
  
  isPrivate ?(
    isUser ? next() : next({name: Sign_in})
  ): console.log('its free!!')
  next()
})

export default router